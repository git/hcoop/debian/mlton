<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>BasisLibrary</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install(2);
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>BasisLibrary</h1>
<div id="toc">
  <div id="toctitle">Table of Contents</div>
  <noscript><p><b>JavaScript must be enabled in your browser to display the table of contents.</b></p></noscript>
</div>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p>The <a href="StandardML">Standard ML</a> Basis Library is a collection of modules
dealing with basic types, input/output, OS interfaces, and simple
datatypes.  It is intended as a portable library usable across all
implementations of SML.  For the official online version of the Basis
Library specification, see <a href="http://www.standardml.org/Basis">http://www.standardml.org/Basis</a>.
<a href="References#GansnerReppy04"> The Standard ML Basis Library</a> is a book
version that includes all of the online version and more.  For a
reverse chronological list of changes to the specification, see
<a href="http://www.standardml.org/Basis/history.html">http://www.standardml.org/Basis/history.html</a>.</p></div>
<div class="paragraph"><p>MLton implements all of the required portions of the Basis Library.
MLton also implements many of the optional structures.  You can obtain
a complete and current list of what&#8217;s available using
<span class="monospaced">mlton -show-basis</span> (see <a href="ShowBasis">ShowBasis</a>).  By default, MLton makes the
Basis Library available to user programs.  You can also
<a href="MLBasisAvailableLibraries">access the Basis Library</a> from
<a href="MLBasis"> ML Basis</a> files.</p></div>
<div class="paragraph"><p>Below is a complete list of what MLton implements.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_top_level_types_and_constructors">Top-level types and constructors</h2>
<div class="sectionbody">
<div class="paragraph"><p><span class="monospaced">eqtype 'a array</span></p></div>
<div class="paragraph"><p><span class="monospaced">datatype bool = false | true</span></p></div>
<div class="paragraph"><p><span class="monospaced">eqtype char</span></p></div>
<div class="paragraph"><p><span class="monospaced">type exn</span></p></div>
<div class="paragraph"><p><span class="monospaced">eqtype int</span></p></div>
<div class="paragraph"><p><span class="monospaced">datatype 'a list = nil | :: of ('a * 'a list)</span></p></div>
<div class="paragraph"><p><span class="monospaced">datatype 'a option = NONE | SOME of 'a</span></p></div>
<div class="paragraph"><p><span class="monospaced">datatype order = EQUAL | GREATER | LESS</span></p></div>
<div class="paragraph"><p><span class="monospaced">type real</span></p></div>
<div class="paragraph"><p><span class="monospaced">datatype 'a ref = ref of 'a</span></p></div>
<div class="paragraph"><p><span class="monospaced">eqtype string</span></p></div>
<div class="paragraph"><p><span class="monospaced">type substring</span></p></div>
<div class="paragraph"><p><span class="monospaced">eqtype unit</span></p></div>
<div class="paragraph"><p><span class="monospaced">eqtype 'a vector</span></p></div>
<div class="paragraph"><p><span class="monospaced">eqtype word</span></p></div>
</div>
</div>
<div class="sect1">
<h2 id="_top_level_exception_constructors">Top-level exception constructors</h2>
<div class="sectionbody">
<div class="paragraph"><p><span class="monospaced">Bind</span></p></div>
<div class="paragraph"><p><span class="monospaced">Chr</span></p></div>
<div class="paragraph"><p><span class="monospaced">Div</span></p></div>
<div class="paragraph"><p><span class="monospaced">Domain</span></p></div>
<div class="paragraph"><p><span class="monospaced">Empty</span></p></div>
<div class="paragraph"><p><span class="monospaced">Fail of string</span></p></div>
<div class="paragraph"><p><span class="monospaced">Match</span></p></div>
<div class="paragraph"><p><span class="monospaced">Option</span></p></div>
<div class="paragraph"><p><span class="monospaced">Overflow</span></p></div>
<div class="paragraph"><p><span class="monospaced">Size</span></p></div>
<div class="paragraph"><p><span class="monospaced">Span</span></p></div>
<div class="paragraph"><p><span class="monospaced">Subscript</span></p></div>
</div>
</div>
<div class="sect1">
<h2 id="_top_level_values">Top-level values</h2>
<div class="sectionbody">
<div class="paragraph"><p>MLton does not implement the optional top-level value
<span class="monospaced">use: string -&gt; unit</span>, which conflicts with whole-program
compilation because it allows new code to be loaded dynamically.</p></div>
<div class="paragraph"><p>MLton implements all other top-level values:</p></div>
<div class="paragraph"><p><span class="monospaced">!</span>,
<span class="monospaced">:=</span>,
<span class="monospaced">&lt;&gt;</span>,
<span class="monospaced">=</span>,
<span class="monospaced">@</span>,
<span class="monospaced">^</span>,
<span class="monospaced">app</span>,
<span class="monospaced">before</span>,
<span class="monospaced">ceil</span>,
<span class="monospaced">chr</span>,
<span class="monospaced">concat</span>,
<span class="monospaced">exnMessage</span>,
<span class="monospaced">exnName</span>,
<span class="monospaced">explode</span>,
<span class="monospaced">floor</span>,
<span class="monospaced">foldl</span>,
<span class="monospaced">foldr</span>,
<span class="monospaced">getOpt</span>,
<span class="monospaced">hd</span>,
<span class="monospaced">ignore</span>,
<span class="monospaced">implode</span>,
<span class="monospaced">isSome</span>,
<span class="monospaced">length</span>,
<span class="monospaced">map</span>,
<span class="monospaced">not</span>,
<span class="monospaced">null</span>,
<span class="monospaced">o</span>,
<span class="monospaced">ord</span>,
<span class="monospaced">print</span>,
<span class="monospaced">real</span>,
<span class="monospaced">rev</span>,
<span class="monospaced">round</span>,
<span class="monospaced">size</span>,
<span class="monospaced">str</span>,
<span class="monospaced">substring</span>,
<span class="monospaced">tl</span>,
<span class="monospaced">trunc</span>,
<span class="monospaced">valOf</span>,
<span class="monospaced">vector</span></p></div>
</div>
</div>
<div class="sect1">
<h2 id="_overloaded_identifiers">Overloaded identifiers</h2>
<div class="sectionbody">
<div class="paragraph"><p><span class="monospaced">*</span>,
<span class="monospaced">+</span>,
<span class="monospaced">-</span>,
<span class="monospaced">/</span>,
<span class="monospaced">&lt;</span>,
<span class="monospaced">&lt;=</span>,
<span class="monospaced">&gt;</span>,
<span class="monospaced">&gt;=</span>,
<span class="monospaced">~</span>,
<span class="monospaced">abs</span>,
<span class="monospaced">div</span>,
<span class="monospaced">mod</span></p></div>
</div>
</div>
<div class="sect1">
<h2 id="_top_level_signatures">Top-level signatures</h2>
<div class="sectionbody">
<div class="paragraph"><p><span class="monospaced">ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">BIN_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">BIT_FLAGS</span></p></div>
<div class="paragraph"><p><span class="monospaced">BOOL</span></p></div>
<div class="paragraph"><p><span class="monospaced">BYTE</span></p></div>
<div class="paragraph"><p><span class="monospaced">CHAR</span></p></div>
<div class="paragraph"><p><span class="monospaced">COMMAND_LINE</span></p></div>
<div class="paragraph"><p><span class="monospaced">DATE</span></p></div>
<div class="paragraph"><p><span class="monospaced">GENERAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">GENERIC_SOCK</span></p></div>
<div class="paragraph"><p><span class="monospaced">IEEE_REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">IMPERATIVE_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">INET_SOCK</span></p></div>
<div class="paragraph"><p><span class="monospaced">INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">INT_INF</span></p></div>
<div class="paragraph"><p><span class="monospaced">IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">LIST</span></p></div>
<div class="paragraph"><p><span class="monospaced">LIST_PAIR</span></p></div>
<div class="paragraph"><p><span class="monospaced">MATH</span></p></div>
<div class="paragraph"><p><span class="monospaced">MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">NET_HOST_DB</span></p></div>
<div class="paragraph"><p><span class="monospaced">NET_PROT_DB</span></p></div>
<div class="paragraph"><p><span class="monospaced">NET_SERV_DB</span></p></div>
<div class="paragraph"><p><span class="monospaced">OPTION</span></p></div>
<div class="paragraph"><p><span class="monospaced">OS</span></p></div>
<div class="paragraph"><p><span class="monospaced">OS_FILE_SYS</span></p></div>
<div class="paragraph"><p><span class="monospaced">OS_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">OS_PATH</span></p></div>
<div class="paragraph"><p><span class="monospaced">OS_PROCESS</span></p></div>
<div class="paragraph"><p><span class="monospaced">PACK_REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">PACK_WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">POSIX</span></p></div>
<div class="paragraph"><p><span class="monospaced">POSIX_ERROR</span></p></div>
<div class="paragraph"><p><span class="monospaced">POSIX_FILE_SYS</span></p></div>
<div class="paragraph"><p><span class="monospaced">POSIX_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">POSIX_PROCESS</span></p></div>
<div class="paragraph"><p><span class="monospaced">POSIX_PROC_ENV</span></p></div>
<div class="paragraph"><p><span class="monospaced">POSIX_SIGNAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">POSIX_SYS_DB</span></p></div>
<div class="paragraph"><p><span class="monospaced">POSIX_TTY</span></p></div>
<div class="paragraph"><p><span class="monospaced">PRIM_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">SOCKET</span></p></div>
<div class="paragraph"><p><span class="monospaced">STREAM_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">STRING</span></p></div>
<div class="paragraph"><p><span class="monospaced">STRING_CVT</span></p></div>
<div class="paragraph"><p><span class="monospaced">SUBSTRING</span></p></div>
<div class="paragraph"><p><span class="monospaced">TEXT</span></p></div>
<div class="paragraph"><p><span class="monospaced">TEXT_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">TEXT_STREAM_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">TIME</span></p></div>
<div class="paragraph"><p><span class="monospaced">TIMER</span></p></div>
<div class="paragraph"><p><span class="monospaced">UNIX</span></p></div>
<div class="paragraph"><p><span class="monospaced">UNIX_SOCK</span></p></div>
<div class="paragraph"><p><span class="monospaced">VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">WORD</span></p></div>
</div>
</div>
<div class="sect1">
<h2 id="_top_level_structures">Top-level structures</h2>
<div class="sectionbody">
<div class="paragraph"><p><span class="monospaced">structure Array: ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Array2: ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure ArraySlice: ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure BinIO: BIN_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure BinPrimIO: PRIM_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Bool: BOOL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure BoolArray: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure BoolArray2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure BoolArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure BoolVector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure BoolVectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Byte: BYTE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Char: CHAR</span></p></div>
<div class="ulist"><ul>
<li>
<p>
<span class="monospaced">Char</span> characters correspond to ISO-8859-1.  The <span class="monospaced">Char</span> functions do not depend on locale.
</p>
</li>
</ul></div>
<div class="paragraph"><p><span class="monospaced">structure CharArray: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure CharArray2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure CharArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure CharVector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure CharVectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure CommandLine: COMMAND_LINE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Date: DATE</span></p></div>
<div class="ulist"><ul>
<li>
<p>
<span class="monospaced">Date.fromString</span> and <span class="monospaced">Date.scan</span> accept a space in addition to a zero for the first character of the day of the month.  The Basis Library specification only allows a zero.
</p>
</li>
</ul></div>
<div class="paragraph"><p><span class="monospaced">structure FixedInt: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure General: GENERAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure GenericSock: GENERIC_SOCK</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure IEEEReal: IEEE_REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure INetSock: INET_SOCK</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure IO: IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int1: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int2: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int3: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int4: INTEGER</span></p></div>
<div class="paragraph"><p>&#8230;</p></div>
<div class="paragraph"><p><span class="monospaced">structure Int31: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int32: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int64: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure IntArray: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure IntArray2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure IntArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure IntVector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure IntVectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int8: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int8Array: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int8Array2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int8ArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int8Vector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int8VectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int16: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int16Array: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int16Array2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int16ArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int16Vector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int16VectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int32: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int32Array: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int32Array2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int32ArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int32Vector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int32VectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int64Array: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int64Array2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int64ArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int64Vector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Int64VectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure IntInf: INT_INF</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeInt: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeIntArray: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeIntArray2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeIntArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeIntVector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeIntVectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeReal: REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeRealArray: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeRealArray2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeRealArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeRealVector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeRealVectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeWord: WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeWordArray: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeWordArray2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeWordArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeWordVector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure LargeWordVectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure List: LIST</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure ListPair: LIST_PAIR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Math: MATH</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure NetHostDB: NET_HOST_DB</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure NetProtDB: NET_PROT_DB</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure NetServDB: NET_SERV_DB</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure OS: OS</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Option: OPTION</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackReal32Big: PACK_REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackReal32Little: PACK_REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackReal64Big: PACK_REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackReal64Little: PACK_REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackRealBig: PACK_REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackRealLittle: PACK_REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackWord16Big: PACK_WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackWord16Little: PACK_WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackWord32Big: PACK_WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackWord32Little: PACK_WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackWord64Big: PACK_WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure PackWord64Little: PACK_WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Position: INTEGER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Posix: POSIX</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real: REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure RealArray: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure RealArray2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure RealArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure RealVector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure RealVectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real32: REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real32Array: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real32Array2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real32ArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real32Vector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real32VectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real64: REAL</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real64Array: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real64Array2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real64ArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real64Vector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Real64VectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Socket: SOCKET</span></p></div>
<div class="ulist"><ul>
<li>
<p>
The Basis Library specification requires functions like
<span class="monospaced">Socket.sendVec</span> to raise an exception if they fail.  However, on some
platforms, sending to a socket that hasn&#8217;t yet been connected causes a
<span class="monospaced">SIGPIPE</span> signal, which invokes the default signal handler for
<span class="monospaced">SIGPIPE</span> and causes the program to terminate.  If you want the
exception to be raised, you can ignore <span class="monospaced">SIGPIPE</span> by adding the
following to your program.
</p>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">let</span><span class="w"></span>
<span class="w">   </span><span class="k">open</span><span class="w"> </span><span class="n">MLton</span><span class="p">.</span><span class="n">Signal</span><span class="w"></span>
<span class="k">in</span><span class="w"></span>
<span class="w">   </span><span class="n">setHandler</span><span class="w"> </span><span class="p">(</span><span class="n">Posix</span><span class="p">.</span><span class="n">Signal</span><span class="p">.</span><span class="n">pipe</span><span class="p">,</span><span class="w"> </span><span class="n">Handler</span><span class="p">.</span><span class="n">ignore</span><span class="p">)</span><span class="w"></span>
<span class="k">end</span><span class="w"></span>
</pre></div></div></div>
</li>
</ul></div>
<div class="paragraph"><p><span class="monospaced">structure String: STRING</span></p></div>
<div class="ulist"><ul>
<li>
<p>
The <span class="monospaced">String</span> functions do not depend on locale.
</p>
</li>
</ul></div>
<div class="paragraph"><p><span class="monospaced">structure StringCvt: STRING_CVT</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Substring: SUBSTRING</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure SysWord: WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Text: TEXT</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure TextIO: TEXT_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure TextPrimIO: PRIM_IO</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Time: TIME</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Timer: TIMER</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Unix: UNIX</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure UnixSock: UNIX_SOCK</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Vector: VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure VectorSlice: VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word: WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word1: WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word2: WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word3: WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word4: WORD</span></p></div>
<div class="paragraph"><p>&#8230;</p></div>
<div class="paragraph"><p><span class="monospaced">structure Word31: WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word32: WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word64: WORD</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure WordArray: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure WordArray2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure WordArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure WordVectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure WordVector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word8Array: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word8Array2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word8ArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word8Vector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word8VectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word16Array: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word16Array2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word16ArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word16Vector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word16VectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word32Array: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word32Array2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word32ArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word32Vector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word32VectorSlice: MONO_VECTOR_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word64Array: MONO_ARRAY</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word64Array2: MONO_ARRAY2</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word64ArraySlice: MONO_ARRAY_SLICE</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word64Vector: MONO_VECTOR</span></p></div>
<div class="paragraph"><p><span class="monospaced">structure Word64VectorSlice: MONO_VECTOR_SLICE</span></p></div>
</div>
</div>
<div class="sect1">
<h2 id="_top_level_functors">Top-level functors</h2>
<div class="sectionbody">
<div class="paragraph"><p><span class="monospaced">ImperativeIO</span></p></div>
<div class="paragraph"><p><span class="monospaced">PrimIO</span></p></div>
<div class="paragraph"><p><span class="monospaced">StreamIO</span></p></div>
<div class="ulist"><ul>
<li>
<p>
MLton&#8217;s <span class="monospaced">StreamIO</span> functor takes structures <span class="monospaced">ArraySlice</span> and
<span class="monospaced">VectorSlice</span> in addition to the arguments specified in the Basis
Library specification.
</p>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_type_equivalences">Type equivalences</h2>
<div class="sectionbody">
<div class="paragraph"><p>The following types are equivalent.</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>FixedInt = Int64.int
LargeInt = IntInf.int
LargeReal.real = Real64.real
LargeWord = Word64.word</pre>
</div></div>
<div class="paragraph"><p>The default <span class="monospaced">int</span>, <span class="monospaced">real</span>, and <span class="monospaced">word</span> types may be set by the
<span class="monospaced">-default-type <em>type</em></span> <a href="CompileTimeOptions"> compile-time option</a>.
By default, the following types are equivalent:</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>int = Int.int = Int32.int
real = Real.real = Real64.real
word = Word.word = Word32.word</pre>
</div></div>
</div>
</div>
<div class="sect1">
<h2 id="_real_and_math_functions">Real and Math functions</h2>
<div class="sectionbody">
<div class="paragraph"><p>The <span class="monospaced">Real</span>, <span class="monospaced">Real32</span>, and <span class="monospaced">Real64</span> modules are implemented
using the <span class="monospaced">C</span> math library, so the SML functions will reflect the
behavior of the underlying library function.  We have made some effort
to unify the differences between the math libraries on different
platforms, and in particular to handle exceptional cases according to
the Basis Library specification.  However, there will be differences
due to different numerical algorithms and cases we may have missed.
Please submit a <a href="Bug">bug report</a> if you encounter an error in
the handling of an exceptional case.</p></div>
<div class="paragraph"><p>On x86, real arithmetic is implemented internally using 80 bits of
precision.  Using higher precision for intermediate results in
computations can lead to different results than if all the computation
is done at 32 or 64 bits.  If you require strict IEEE compliance, you
can compile with <span class="monospaced">-ieee-fp true</span>, which will cause intermediate
results to be stored after each operation.  This may cause a
substantial performance penalty.</p></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
