<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>History</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>History</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p>In April 1997, Stephen Weeks wrote a defunctorizer for Standard ML and
integrated it with SML/NJ.  The defunctorizer used SML/NJ&#8217;s visible
compiler and operated on the <span class="monospaced">Ast</span> intermediate representation
produced by the SML/NJ front end.  Experiments showed that
defunctorization gave a speedup of up to six times over separate
compilation and up to two times over batch compilation without functor
expansion.</p></div>
<div class="paragraph"><p>In August 1997, we began development of an independent compiler for
SML.  At the time the compiler was called <span class="monospaced">smlc</span>.  By October, we had
a working monomorphiser.  By November, we added a polyvariant
higher-order control-flow analysis.  At that point, MLton was about
10,000 lines of code.</p></div>
<div class="paragraph"><p>Over the next year and half, <span class="monospaced">smlc</span> morphed into a full-fledged
compiler for SML.  It was renamed MLton, and first released in March
1999.</p></div>
<div class="paragraph"><p>From the start, MLton has been driven by whole-program optimization
and an emphasis on performance.  Also from the start, MLton has had a
fast C FFI and <span class="monospaced">IntInf</span> based on the GNU multiprecision library.  At
its first release, MLton was 48,006 lines.</p></div>
<div class="paragraph"><p>Between the March 1999 and January 2002, MLton grew to 102,541 lines,
as we added a native code generator, mllex, mlyacc, a profiler, many
optimizations, and many libraries including threads and signal
handling.</p></div>
<div class="paragraph"><p>During 2002, MLton grew to 112,204 lines and we had releases in April
and September.  We added support for cross compilation and used this
to enable MLton to run on Cygwin/Windows and FreeBSD.  We also made
improvements to the garbage collector, so that it now works with large
arrays and up to 4G of memory and so that it automatically uses
copying, mark-compact, or generational collection depending on heap
usage and RAM size.  We also continued improvements to the optimizer
and libraries.</p></div>
<div class="paragraph"><p>During 2003, MLton grew to 122,299 lines and we had releases in March
and July.  We extended the profiler to support source-level profiling
of time and allocation and to display call graphs.  We completed the
Basis Library implementation, and added new MLton-specific libraries
for weak pointers and finalization.  We extended the FFI to allow
callbacks from C to SML.  We added support for the Sparc/Solaris
platform, and made many improvements to the C code generator.</p></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
