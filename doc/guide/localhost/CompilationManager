<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>CompilationManager</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>CompilationManager</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p>The <a href="http://www.smlnj.org/doc/CM/index.html">Compilation Manager</a> (CM) is SML/NJ&#8217;s mechanism for supporting programming-in-the-very-large.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_porting_sml_nj_cm_files_to_mlton">Porting SML/NJ CM files to MLton</h2>
<div class="sectionbody">
<div class="paragraph"><p>To help in porting CM files to MLton, the MLton source distribution
includes the sources for a utility, <span class="monospaced">cm2mlb</span>, that will print an
<a href="MLBasis"> ML Basis</a> file with essentially the same semantics as the
CM file&#8201;&#8212;&#8201;handling the full syntax of CM supported by your installed
SML/NJ version and correctly handling export filters.  When <span class="monospaced">cm2mlb</span>
encounters a <span class="monospaced">.cm</span> import, it attempts to convert it to a
corresponding <span class="monospaced">.mlb</span> import.  CM anchored paths are translated to
paths according to a default configuration file
(<a href="https://github.com/MLton/mlton/blob/master/util/cm2mlb/cm2mlb-map"><span class="monospaced">cm2mlb-map</span></a>). For example,
the default configuration includes</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre># Standard ML Basis Library
$SMLNJ-BASIS                            $(SML_LIB)/basis
$basis.cm                               $(SML_LIB)/basis
$basis.cm/basis.cm                      $(SML_LIB)/basis/basis.mlb</pre>
</div></div>
<div class="paragraph"><p>to ensure that a <span class="monospaced">$/basis.cm</span> import is translated to a
<span class="monospaced">$(SML_LIB)/basis/basis.mlb</span> import.  See <span class="monospaced">util/cm2mlb</span> for details.
Building <span class="monospaced">cm2mlb</span> requires that you have already installed a recent
version of SML/NJ.</p></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
