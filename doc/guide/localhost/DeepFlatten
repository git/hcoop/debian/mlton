<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>DeepFlatten</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>DeepFlatten</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p><a href="DeepFlatten">DeepFlatten</a> is an optimization pass for the <a href="SSA2">SSA2</a>
<a href="IntermediateLanguage">IntermediateLanguage</a>, invoked from <a href="SSA2Simplify">SSA2Simplify</a>.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_description">Description</h2>
<div class="sectionbody">
<div class="paragraph"><p>This pass flattens into mutable fields of objects and into vectors.</p></div>
<div class="paragraph"><p>For example, an <span class="monospaced">(int * int) ref</span> is represented by a 2 word
object, and an <span class="monospaced">(int * int) array</span> contains pairs of <span class="monospaced">int</span>-s,
rather than pointers to pairs of <span class="monospaced">int</span>-s.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_implementation">Implementation</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
<a href="https://github.com/MLton/mlton/blob/master/mlton/ssa/deep-flatten.fun"><span class="monospaced">deep-flatten.fun</span></a>
</p>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_details_and_notes">Details and Notes</h2>
<div class="sectionbody">
<div class="paragraph"><p>There are some performance issues with the deep flatten pass, where it
consumes an excessive amount of memory.</p></div>
<div class="ulist"><ul>
<li>
<p>
<a href="http://www.mlton.org/pipermail/mlton/2005-April/026990.html">http://www.mlton.org/pipermail/mlton/2005-April/026990.html</a>
</p>
</li>
<li>
<p>
<a href="http://www.mlton.org/pipermail/mlton-user/2010-June/001626.html">http://www.mlton.org/pipermail/mlton-user/2010-June/001626.html</a>
</p>
</li>
<li>
<p>
<a href="http://www.mlton.org/pipermail/mlton/2010-December/030876.html">http://www.mlton.org/pipermail/mlton/2010-December/030876.html</a>
</p>
</li>
</ul></div>
<div class="paragraph"><p>A number of applications require compilation with
<span class="monospaced">-disable-pass deepFlatten</span> to avoid exceeding available memory.  It is
often asked whether the deep flatten pass usually has a significant
impact on performance.  The standard benchmark suite was run with and
without the deep flatten pass enabled when the pass was first
introduced:</p></div>
<div class="ulist"><ul>
<li>
<p>
<a href="http://www.mlton.org/pipermail/mlton/2004-August/025760.html">http://www.mlton.org/pipermail/mlton/2004-August/025760.html</a>
</p>
</li>
</ul></div>
<div class="paragraph"><p>The conclusion is that it does not have a significant impact.
However, these are micro benchmarks; other applications may derive
greater benefit from the pass.</p></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
