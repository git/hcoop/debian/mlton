<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>MLtonSignal</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>MLtonSignal</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">signature</span><span class="w"> </span><span class="n">MLTON_SIGNAL</span><span class="w"> </span><span class="p">=</span><span class="w"></span>
<span class="w">   </span><span class="k">sig</span><span class="w"></span>
<span class="w">      </span><span class="k">type</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">Posix</span><span class="p">.</span><span class="n">Signal</span><span class="p">.</span><span class="n">signal</span><span class="w"></span>
<span class="w">      </span><span class="k">type</span><span class="w"> </span><span class="n">signal</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">t</span><span class="w"></span>

<span class="w">      </span><span class="k">structure</span><span class="w"> </span><span class="n">Handler</span><span class="p">:</span><span class="w"></span>
<span class="w">         </span><span class="k">sig</span><span class="w"></span>
<span class="w">            </span><span class="k">type</span><span class="w"> </span><span class="n">t</span><span class="w"></span>

<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">default</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">handler</span><span class="p">:</span><span class="w"> </span><span class="p">(</span><span class="n">Thread</span><span class="p">.</span><span class="n">Runnable</span><span class="p">.</span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">Thread</span><span class="p">.</span><span class="n">Runnable</span><span class="p">.</span><span class="n">t</span><span class="p">)</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">ignore</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">isDefault</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">bool</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">isIgnore</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">bool</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">simple</span><span class="p">:</span><span class="w"> </span><span class="p">(</span><span class="n">unit</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">unit</span><span class="p">)</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">         </span><span class="k">end</span><span class="w"></span>

<span class="w">      </span><span class="k">structure</span><span class="w"> </span><span class="n">Mask</span><span class="p">:</span><span class="w"></span>
<span class="w">         </span><span class="k">sig</span><span class="w"></span>
<span class="w">            </span><span class="k">type</span><span class="w"> </span><span class="n">t</span><span class="w"></span>

<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">all</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">allBut</span><span class="p">:</span><span class="w"> </span><span class="n">signal</span><span class="w"> </span><span class="n">list</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">block</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">unit</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">getBlocked</span><span class="p">:</span><span class="w"> </span><span class="n">unit</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">isMember</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">signal</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">bool</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">none</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">setBlocked</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">unit</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">some</span><span class="p">:</span><span class="w"> </span><span class="n">signal</span><span class="w"> </span><span class="n">list</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">            </span><span class="k">val</span><span class="w"> </span><span class="n">unblock</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">unit</span><span class="w"></span>
<span class="w">         </span><span class="k">end</span><span class="w"></span>

<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">getHandler</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">Handler</span><span class="p">.</span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">handled</span><span class="p">:</span><span class="w"> </span><span class="n">unit</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">Mask</span><span class="p">.</span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">prof</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">restart</span><span class="p">:</span><span class="w"> </span><span class="n">bool</span><span class="w"> </span><span class="n">ref</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">setHandler</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">Handler</span><span class="p">.</span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">unit</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">suspend</span><span class="p">:</span><span class="w"> </span><span class="n">Mask</span><span class="p">.</span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">unit</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">vtalrm</span><span class="p">:</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">   </span><span class="k">end</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>Signals handlers are functions from (runnable) threads to (runnable)
threads.  When a signal arrives, the corresponding signal handler is
invoked, its argument being the thread that was interrupted by the
signal.  The signal handler runs asynchronously, in its own thread.
The signal handler returns the thread that it would like to resume
execution (this is often the thread that it was passed).  It is an
error for a signal handler to raise an exception that is not handled
within the signal handler itself.</p></div>
<div class="paragraph"><p>A signal handler is never invoked while the running thread is in a
critical section (see <a href="MLtonThread">MLtonThread</a>).  Invoking a signal handler
implicitly enters a critical section and the normal return of a signal
handler implicitly exits the critical section; hence, a signal handler
is never interrupted by another signal handler.</p></div>
<div class="ulist"><ul>
<li>
<p>
<span class="monospaced">type t</span>
</p>
<div class="paragraph"><p>the type of signals.</p></div>
</li>
<li>
<p>
<span class="monospaced">type Handler.t</span>
</p>
<div class="paragraph"><p>the type of signal handlers.</p></div>
</li>
<li>
<p>
<span class="monospaced">Handler.default</span>
</p>
<div class="paragraph"><p>handles the signal with the default action.</p></div>
</li>
<li>
<p>
<span class="monospaced">Handler.handler f</span>
</p>
<div class="paragraph"><p>returns a handler <span class="monospaced">h</span> such that when a signal <span class="monospaced">s</span> is handled by <span class="monospaced">h</span>,
<span class="monospaced">f</span> will be passed the thread that was interrupted by <span class="monospaced">s</span> and should
return the thread that will resume execution.</p></div>
</li>
<li>
<p>
<span class="monospaced">Handler.ignore</span>
</p>
<div class="paragraph"><p>is a handler that will ignore the signal.</p></div>
</li>
<li>
<p>
<span class="monospaced">Handler.isDefault</span>
</p>
<div class="paragraph"><p>returns true if the handler is the default handler.</p></div>
</li>
<li>
<p>
<span class="monospaced">Handler.isIgnore</span>
</p>
<div class="paragraph"><p>returns true if the handler is the ignore handler.</p></div>
</li>
<li>
<p>
<span class="monospaced">Handler.simple f</span>
</p>
<div class="paragraph"><p>returns a handler that executes <span class="monospaced">f ()</span> and does not switch threads.</p></div>
</li>
<li>
<p>
<span class="monospaced">type Mask.t</span>
</p>
<div class="paragraph"><p>the type of signal masks, which are sets of blocked signals.</p></div>
</li>
<li>
<p>
<span class="monospaced">Mask.all</span>
</p>
<div class="paragraph"><p>a mask of all signals.</p></div>
</li>
<li>
<p>
<span class="monospaced">Mask.allBut l</span>
</p>
<div class="paragraph"><p>a mask of all signals except for those in <span class="monospaced">l</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">Mask.block m</span>
</p>
<div class="paragraph"><p>blocks all signals in <span class="monospaced">m</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">Mask.getBlocked ()</span>
</p>
<div class="paragraph"><p>gets the signal mask <span class="monospaced">m</span>, i.e. a signal is blocked if and only if it
is in <span class="monospaced">m</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">Mask.isMember (m, s)</span>
</p>
<div class="paragraph"><p>returns true if the signal <span class="monospaced">s</span> is in <span class="monospaced">m</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">Mask.none</span>
</p>
<div class="paragraph"><p>a mask of no signals.</p></div>
</li>
<li>
<p>
<span class="monospaced">Mask.setBlocked m</span>
</p>
<div class="paragraph"><p>sets the signal mask to <span class="monospaced">m</span>, i.e. a signal is blocked if and only if
it is in <span class="monospaced">m</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">Mask.some l</span>
</p>
<div class="paragraph"><p>a mask of the signals in <span class="monospaced">l</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">Mask.unblock m</span>
</p>
<div class="paragraph"><p>unblocks all signals in <span class="monospaced">m</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">getHandler s</span>
</p>
<div class="paragraph"><p>returns the current handler for signal <span class="monospaced">s</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">handled ()</span>
</p>
<div class="paragraph"><p>returns the signal mask <span class="monospaced">m</span> corresponding to the currently handled
signals; i.e., a signal is handled if and only if it is in <span class="monospaced">m</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">prof</span>
</p>
<div class="paragraph"><p><span class="monospaced">SIGPROF</span>, the profiling signal.</p></div>
</li>
<li>
<p>
<span class="monospaced">restart</span>
</p>
<div class="paragraph"><p>dynamically determines the behavior of interrupted system calls; when
<span class="monospaced">true</span>, interrupted system calls are restarted; when <span class="monospaced">false</span>,
interrupted system calls raise <span class="monospaced">OS.SysError</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">setHandler (s, h)</span>
</p>
<div class="paragraph"><p>sets the handler for signal <span class="monospaced">s</span> to <span class="monospaced">h</span>.</p></div>
</li>
<li>
<p>
<span class="monospaced">suspend m</span>
</p>
<div class="paragraph"><p>temporarily sets the signal mask to <span class="monospaced">m</span> and suspends until an unmasked
signal is received and handled, at which point <span class="monospaced">suspend</span> resets the
mask and returns.</p></div>
</li>
<li>
<p>
<span class="monospaced">vtalrm</span>
</p>
<div class="paragraph"><p><span class="monospaced">SIGVTALRM</span>, the signal for virtual timers.</p></div>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_interruptible_system_calls">Interruptible System Calls</h2>
<div class="sectionbody">
<div class="paragraph"><p>Signal handling interacts in a non-trivial way with those functions in
the <a href="BasisLibrary">Basis Library</a> that correspond directly to
interruptible system calls (a subset of those functions that may raise
<span class="monospaced">OS.SysError</span>).  The desire is that these functions should have
predictable semantics.  The principal concerns are:</p></div>
<div class="olist arabic"><ol class="arabic">
<li>
<p>
System calls that are interrupted by signals should, by default, be
restarted; the alternative is to raise
</p>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="n">OS</span><span class="p">.</span><span class="n">SysError</span><span class="w"> </span><span class="p">(</span><span class="n">Posix</span><span class="p">.</span><span class="n">Error</span><span class="p">.</span><span class="n">errorMsg</span><span class="w"> </span><span class="n">Posix</span><span class="p">.</span><span class="n">Error</span><span class="p">.</span><span class="n">intr</span><span class="p">,</span><span class="w"></span>
<span class="w">             </span><span class="n">SOME</span><span class="w"> </span><span class="n">Posix</span><span class="p">.</span><span class="n">Error</span><span class="p">.</span><span class="n">intr</span><span class="p">)</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>This behavior is determined dynamically by the value of <span class="monospaced">Signal.restart</span>.</p></div>
</li>
<li>
<p>
Signal handlers should always get a chance to run (when outside a
critical region).  If a system call is interrupted by a signal, then
the signal handler will run before the call is restarted or
<span class="monospaced">OS.SysError</span> is raised; that is, before the <span class="monospaced">Signal.restart</span> check.
</p>
</li>
<li>
<p>
A system call that must be restarted while in a critical section
will be restarted with the handled signals blocked (and the previously
blocked signals remembered).  This encourages the system call to
complete, allowing the program to make progress towards leaving the
critical section where the signal can be handled.  If the system call
completes, the set of blocked signals are restored to those previously
blocked.
</p>
</li>
</ol></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
