<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>VariableArityPolymorphism</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>VariableArityPolymorphism</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p><a href="StandardML">Standard ML</a> programmers often face the problem of how to
provide a variable-arity polymorphic function.  For example, suppose
one is defining a combinator library, e.g. for parsing or pickling.
The signature for such a library might look something like the
following.</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">signature</span><span class="w"> </span><span class="n">COMBINATOR</span><span class="w"> </span><span class="p">=</span><span class="w"></span>
<span class="w">   </span><span class="k">sig</span><span class="w"></span>
<span class="w">      </span><span class="k">type</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">t</span><span class="w"></span>

<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">int</span><span class="p">:</span><span class="w"> </span><span class="n">int</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">real</span><span class="p">:</span><span class="w"> </span><span class="n">real</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">string</span><span class="p">:</span><span class="w"> </span><span class="n">string</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">unit</span><span class="p">:</span><span class="w"> </span><span class="n">unit</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">tuple2</span><span class="p">:</span><span class="w"> </span><span class="n">&#39;a1</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a2</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a1</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a2</span><span class="p">)</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">tuple3</span><span class="p">:</span><span class="w"> </span><span class="n">&#39;a1</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a2</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a3</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a1</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a2</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a3</span><span class="p">)</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">tuple4</span><span class="p">:</span><span class="w"> </span><span class="n">&#39;a1</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a2</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a3</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a4</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">                  </span><span class="p">-&gt;</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a1</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a2</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a3</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;a4</span><span class="p">)</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="p">...</span><span class="w"></span>
<span class="w">   </span><span class="k">end</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>The question is how to define a variable-arity tuple combinator.
Traditionally, the only way to take a variable number of arguments in
SML is to put the arguments in a list (or vector) and pass that.  So,
one might define a tuple combinator with the following signature.</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">val</span><span class="w"> </span><span class="n">tupleN</span><span class="p">:</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">list</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">list</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>The problem with this approach is that as soon as one places values in
a list, they must all have the same type.  So, programmers often take
an alternative approach, and define a family of <span class="monospaced">tuple&lt;N&gt;</span> functions,
as we see in the <span class="monospaced">COMBINATOR</span> signature above.</p></div>
<div class="paragraph"><p>The family-of-functions approach is ugly for many reasons.  First, it
clutters the signature with a number of functions when there should
really only be one.  Second, it is <em>closed</em>, in that there are a fixed
number of tuple combinators in the interface, and should a client need
a combinator for a large tuple, he is out of luck.  Third, this
approach often requires a lot of duplicate code in the implementation
of the combinators.</p></div>
<div class="paragraph"><p>Fortunately, using <a href="Fold01N">Fold01N</a> and <a href="ProductType">products</a>, one can
provide an interface and implementation that solves all these
problems.  Here is a simple pickling module that converts values to
strings.</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">structure</span><span class="w"> </span><span class="n">Pickler</span><span class="w"> </span><span class="p">=</span><span class="w"></span>
<span class="w">   </span><span class="k">struct</span><span class="w"></span>
<span class="w">      </span><span class="k">type</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">string</span><span class="w"></span>

<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">unit</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="k">fn</span><span class="w"> </span><span class="p">()</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="s">&quot;&quot;</span><span class="w"></span>

<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">int</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">Int</span><span class="p">.</span><span class="n">toString</span><span class="w"></span>

<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">real</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">Real</span><span class="p">.</span><span class="n">toString</span><span class="w"></span>

<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">string</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">id</span><span class="w"></span>

<span class="w">      </span><span class="k">type</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">accum</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">string</span><span class="w"> </span><span class="n">list</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">string</span><span class="w"> </span><span class="n">list</span><span class="w"></span>

<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">tuple</span><span class="w"> </span><span class="p">=</span><span class="w"></span>
<span class="w">         </span><span class="k">fn</span><span class="w"> </span><span class="n">z</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"></span>
<span class="w">         </span><span class="n">Fold01N</span><span class="p">.</span><span class="n">fold</span><span class="w"></span>
<span class="w">         </span><span class="p">{</span><span class="n">finish</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="k">fn</span><span class="w"> </span><span class="n">ps</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="k">fn</span><span class="w"> </span><span class="n">x</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="n">concat</span><span class="w"> </span><span class="p">(</span><span class="n">rev</span><span class="w"> </span><span class="p">(</span><span class="n">ps</span><span class="w"> </span><span class="p">(</span><span class="n">x</span><span class="p">,</span><span class="w"> </span><span class="p">[]))),</span><span class="w"></span>
<span class="w">          </span><span class="n">start</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="k">fn</span><span class="w"> </span><span class="n">p</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="k">fn</span><span class="w"> </span><span class="p">(</span><span class="n">x</span><span class="p">,</span><span class="w"> </span><span class="n">l</span><span class="p">)</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="n">p</span><span class="w"> </span><span class="n">x</span><span class="w"> </span><span class="n">::</span><span class="w"> </span><span class="n">l</span><span class="p">,</span><span class="w"></span>
<span class="w">          </span><span class="n">zero</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">unit</span><span class="p">}</span><span class="w"></span>
<span class="w">         </span><span class="n">z</span><span class="w"></span>

<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">`</span><span class="w"> </span><span class="p">=</span><span class="w"></span>
<span class="w">         </span><span class="k">fn</span><span class="w"> </span><span class="n">z</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"></span>
<span class="w">         </span><span class="n">Fold01N</span><span class="p">.</span><span class="n">step1</span><span class="w"></span>
<span class="w">         </span><span class="p">{</span><span class="n">combine</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="p">(</span><span class="k">fn</span><span class="w"> </span><span class="p">(</span><span class="n">p</span><span class="p">,</span><span class="w"> </span><span class="n">p&#39;</span><span class="p">)</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="k">fn</span><span class="w"> </span><span class="p">(</span><span class="n">x</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="n">x&#39;</span><span class="p">,</span><span class="w"> </span><span class="n">l</span><span class="p">)</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="n">p&#39;</span><span class="w"> </span><span class="n">x&#39;</span><span class="w"> </span><span class="n">::</span><span class="w"> </span><span class="s">&quot;,&quot;</span><span class="w"> </span><span class="n">::</span><span class="w"> </span><span class="n">p</span><span class="w"> </span><span class="p">(</span><span class="n">x</span><span class="p">,</span><span class="w"> </span><span class="n">l</span><span class="p">))}</span><span class="w"></span>
<span class="w">         </span><span class="n">z</span><span class="w"></span>
<span class="w">   </span><span class="k">end</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>If one has <span class="monospaced">n</span> picklers of types</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">val</span><span class="w"> </span><span class="n">p1</span><span class="p">:</span><span class="w"> </span><span class="n">a1</span><span class="w"> </span><span class="n">Pickler</span><span class="p">.</span><span class="n">t</span><span class="w"></span>
<span class="k">val</span><span class="w"> </span><span class="n">p2</span><span class="p">:</span><span class="w"> </span><span class="n">a2</span><span class="w"> </span><span class="n">Pickler</span><span class="p">.</span><span class="n">t</span><span class="w"></span>
<span class="p">...</span><span class="w"></span>
<span class="k">val</span><span class="w"> </span><span class="n">pn</span><span class="p">:</span><span class="w"> </span><span class="n">an</span><span class="w"> </span><span class="n">Pickler</span><span class="p">.</span><span class="n">t</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>then one can construct a pickler for n-ary products as follows.</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="n">tuple</span><span class="w"> </span><span class="n">`p1</span><span class="w"> </span><span class="n">`p2</span><span class="w"> </span><span class="p">...</span><span class="w"> </span><span class="n">`pn</span><span class="w"> </span><span class="n">$</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="p">(</span><span class="n">a1</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="n">a2</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="p">...</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="n">an</span><span class="p">)</span><span class="w"> </span><span class="n">Pickler</span><span class="p">.</span><span class="n">t</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>For example, with <span class="monospaced">Pickler</span> in scope, one can prove the following
equations.</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="s">&quot;&quot;</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">tuple</span><span class="w"> </span><span class="n">$</span><span class="w"> </span><span class="p">()</span><span class="w"></span>
<span class="s">&quot;1&quot;</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">tuple</span><span class="w"> </span><span class="n">`int</span><span class="w"> </span><span class="n">$</span><span class="w"> </span><span class="mi">1</span><span class="w"></span>
<span class="s">&quot;1,2.0&quot;</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">tuple</span><span class="w"> </span><span class="n">`int</span><span class="w"> </span><span class="n">`real</span><span class="w"> </span><span class="n">$</span><span class="w"> </span><span class="p">(</span><span class="mi">1</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="mf">2.0</span><span class="p">)</span><span class="w"></span>
<span class="s">&quot;1,2.0,three&quot;</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">tuple</span><span class="w"> </span><span class="n">`int</span><span class="w"> </span><span class="n">`real</span><span class="w"> </span><span class="n">`string</span><span class="w"> </span><span class="n">$</span><span class="w"> </span><span class="p">(</span><span class="mi">1</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="mf">2.0</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="s">&quot;three&quot;</span><span class="p">)</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>Here is the signature for <span class="monospaced">Pickler</span>.  It shows why the <span class="monospaced">accum</span> type is
useful.</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">signature</span><span class="w"> </span><span class="n">PICKLER</span><span class="w"> </span><span class="p">=</span><span class="w"></span>
<span class="w">   </span><span class="k">sig</span><span class="w"></span>
<span class="w">      </span><span class="k">type</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">t</span><span class="w"></span>

<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">int</span><span class="p">:</span><span class="w"> </span><span class="n">int</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">real</span><span class="p">:</span><span class="w"> </span><span class="n">real</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">string</span><span class="p">:</span><span class="w"> </span><span class="n">string</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">unit</span><span class="p">:</span><span class="w"> </span><span class="n">unit</span><span class="w"> </span><span class="n">t</span><span class="w"></span>

<span class="w">      </span><span class="k">type</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">accum</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">`</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="w"> </span><span class="n">accum</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;b</span><span class="w"> </span><span class="n">t</span><span class="p">,</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;b</span><span class="p">)</span><span class="w"> </span><span class="n">prod</span><span class="w"> </span><span class="n">accum</span><span class="p">,</span><span class="w"></span>
<span class="w">               </span><span class="n">&#39;z1</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;z2</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;z3</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;z4</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;z5</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;z6</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;z7</span><span class="p">)</span><span class="w"> </span><span class="n">Fold01N</span><span class="p">.</span><span class="n">step1</span><span class="w"></span>
<span class="w">      </span><span class="k">val</span><span class="w"> </span><span class="n">tuple</span><span class="p">:</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="w"> </span><span class="n">t</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">accum</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;b</span><span class="w"> </span><span class="n">accum</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;b</span><span class="w"> </span><span class="n">t</span><span class="p">,</span><span class="w"> </span><span class="n">unit</span><span class="w"> </span><span class="n">t</span><span class="p">,</span><span class="w"></span>
<span class="w">                  </span><span class="n">&#39;z1</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;z2</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;z3</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;z4</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;z5</span><span class="p">)</span><span class="w"> </span><span class="n">Fold01N</span><span class="p">.</span><span class="n">t</span><span class="w"></span>
<span class="w">   </span><span class="k">end</span><span class="w"></span>

<span class="k">structure</span><span class="w"> </span><span class="n">Pickler</span><span class="p">:</span><span class="w"> </span><span class="n">PICKLER</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">Pickler</span><span class="w"></span>
</pre></div></div></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
