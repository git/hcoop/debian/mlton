<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>MLBasis</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>MLBasis</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p>The ML Basis system extends <a href="StandardML">Standard ML</a> to support
programming-in-the-very-large, namespace management at the module
level, separate delivery of library sources, and more.  While Standard
ML modules are a sophisticated language for programming-in-the-large,
it is difficult, if not impossible, to accomplish a number of routine
namespace management operations when a program draws upon multiple
libraries provided by different vendors.</p></div>
<div class="paragraph"><p>The ML Basis system is a simple, yet powerful, approach that builds
upon the programmer&#8217;s intuitive notion (and
<a href="DefinitionOfStandardML"> The Definition of Standard ML (Revised)</a>'s
formal notion) of the top-level environment (a <em>basis</em>).  The system
is designed as a natural extension of <a href="StandardML"> Standard ML</a>; the
formal specification of the ML Basis system
(<a href="MLBasis.attachments/mlb-formal.pdf"><span class="monospaced">mlb-formal.pdf</span></a>) is given in the style
of the Definition.</p></div>
<div class="paragraph"><p>Here are some of the key features of the ML Basis system:</p></div>
<div class="olist arabic"><ol class="arabic">
<li>
<p>
Explicit file order: The order of files (and, hence, the order of
evaluation) in the program is explicit.  The ML Basis system&#8217;s
semantics are structured in such a way that for any well-formed
project, there will be exactly one possible interpretation of the
project&#8217;s syntax, static semantics, and dynamic semantics.
</p>
</li>
<li>
<p>
Implicit dependencies: A source file (corresponding to an SML
top-level declaration) is elaborated in the environment described by
preceding declarations.  It is not necessary to explicitly list the
dependencies of a file.
</p>
</li>
<li>
<p>
Scoping and renaming: The ML Basis system provides mechanisms for
limiting the scope of (i.e, hiding) and renaming identifiers.
</p>
</li>
<li>
<p>
No naming convention for finding the file that defines a module.
To import a module, its defining file must appear in some ML Basis
file.
</p>
</li>
</ol></div>
</div>
</div>
<div class="sect1">
<h2 id="_next_steps">Next steps</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
<a href="MLBasisSyntaxAndSemantics">MLBasisSyntaxAndSemantics</a>
</p>
</li>
<li>
<p>
<a href="MLBasisExamples">MLBasisExamples</a>
</p>
</li>
<li>
<p>
<a href="MLBasisPathMap">MLBasisPathMap</a>
</p>
</li>
<li>
<p>
<a href="MLBasisAnnotations">MLBasisAnnotations</a>
</p>
</li>
<li>
<p>
<a href="MLBasisAvailableLibraries">MLBasisAvailableLibraries</a>
</p>
</li>
</ul></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
