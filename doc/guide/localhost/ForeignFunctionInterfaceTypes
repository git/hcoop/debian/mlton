<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>ForeignFunctionInterfaceTypes</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>ForeignFunctionInterfaceTypes</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p>MLton&#8217;s <a href="ForeignFunctionInterface">ForeignFunctionInterface</a> only allows values of certain SML
types to be passed between SML and C.  The following types are
allowed: <span class="monospaced">bool</span>, <span class="monospaced">char</span>, <span class="monospaced">int</span>, <span class="monospaced">real</span>, <span class="monospaced">word</span>.  All of the different
sizes of (fixed-sized) integers, reals, and words are supported as
well: <span class="monospaced">Int8.int</span>, <span class="monospaced">Int16.int</span>, <span class="monospaced">Int32.int</span>, <span class="monospaced">Int64.int</span>,
<span class="monospaced">Real32.real</span>, <span class="monospaced">Real64.real</span>, <span class="monospaced">Word8.word</span>, <span class="monospaced">Word16.word</span>,
<span class="monospaced">Word32.word</span>, <span class="monospaced">Word64.word</span>.  There is a special type,
<span class="monospaced">MLton.Pointer.t</span>, for passing C pointers&#8201;&#8212;&#8201;see <a href="MLtonPointer">MLtonPointer</a> for
details.</p></div>
<div class="paragraph"><p>Arrays, refs, and vectors of the above types are also allowed.
Because in MLton monomorphic arrays and vectors are exactly the same
as their polymorphic counterpart, these are also allowed.  Hence,
<span class="monospaced">string</span>, <span class="monospaced">char vector</span>, and <span class="monospaced">CharVector.vector</span> are also allowed.
Strings are not null terminated, unless you manually do so from the
SML side.</p></div>
<div class="paragraph"><p>Unfortunately, passing tuples or datatypes is not allowed because that
would interfere with representation optimizations.</p></div>
<div class="paragraph"><p>The C header file that <span class="monospaced">-export-header</span> generates includes
<span class="monospaced">typedef</span>s for the C types corresponding to the SML types.  Here is
the mapping between SML types and C types.</p></div>
<table class="tableblock frame-all grid-all"
style="
width:100%;
">
<col style="width:25%;">
<col style="width:25%;">
<col style="width:25%;">
<col style="width:25%;">
<thead>
<tr>
<th class="tableblock halign-left valign-top" > SML type </th>
<th class="tableblock halign-left valign-top" > C typedef </th>
<th class="tableblock halign-left valign-top" > C type </th>
<th class="tableblock halign-left valign-top" > Note</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">array</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Pointer</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">unsigned char *</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">bool</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Bool</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">int32_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">char</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Char8</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">uint8_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Int8.int</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Int8</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">int8_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Int16.int</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Int16</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">int16_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Int32.int</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Int32</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">int32_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Int64.int</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Int64</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">int64_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">int</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Int32</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">int32_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><a href="ForeignFunctionInterfaceTypes#Default">(default)</a></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">MLton.Pointer.t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Pointer</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">unsigned char *</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Real32.real</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Real32</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">float</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Real64.real</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Real64</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">double</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">real</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Real64</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">double</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><a href="ForeignFunctionInterfaceTypes#Default">(default)</a></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">ref</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Pointer</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">unsigned char *</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">string</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Pointer</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">unsigned char *</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><a href="ForeignFunctionInterfaceTypes#ReadOnly">(read only)</a></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">vector</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Pointer</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">unsigned char *</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><a href="ForeignFunctionInterfaceTypes#ReadOnly">(read only)</a></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Word8.word</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Word8</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">uint8_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Word16.word</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Word16</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">uint16_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Word32.word</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Word32</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">uint32_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Word64.word</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Word64</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">uint64_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">word</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">Word32</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">uint32_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><a href="ForeignFunctionInterfaceTypes#Default">(default)</a></p></td>
</tr>
</tbody>
</table>
<div class="paragraph"><p><a id="Default"></a>Note (default): The default <span class="monospaced">int</span>, <span class="monospaced">real</span>, and
<span class="monospaced">word</span> types may be set by the <span class="monospaced">-default-type <em>type</em></span>
<a href="CompileTimeOptions"> compiler option</a>.  The given C typedef and C
types correspond to the default behavior.</p></div>
<div class="paragraph"><p><a id="ReadOnly"></a>Note (read only): Because MLton assumes that
vectors and strings are read-only (and will perform optimizations
that, for instance, cause them to share space), you must not modify
the data pointed to by the <span class="monospaced">unsigned char *</span> in C code.</p></div>
<div class="paragraph"><p>Although the C type of an array, ref, or vector is always <span class="monospaced">Pointer</span>,
in reality, the object has the natural C representation.  Your C code
should cast to the appropriate C type if you want to keep the C
compiler from complaining.</p></div>
<div class="paragraph"><p>When calling an <a href="CallingFromSMLToC"> imported C function from SML</a>
that returns an array, ref, or vector result or when calling an
<a href="CallingFromCToSML"> exported SML function from C</a> that takes an
array, ref, or string argument, then the object must be an ML object
allocated on the ML heap.  (Although an array, ref, or vector object
has the natural C representation, the object also has an additional
header used by the SML runtime system.)</p></div>
<div class="paragraph"><p>In addition, there is an <a href="MLBasis">MLBasis</a> file, <span class="monospaced">$(SML_LIB)/basis/c-types.mlb</span>,
which provides structure aliases for various C types:</p></div>
<table class="tableblock frame-all grid-all"
style="
width:100%;
">
<col style="width:33%;">
<col style="width:33%;">
<col style="width:33%;">
<tbody>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock">C type</p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock">Structure</p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock">Signature</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">char</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Char</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">signed char</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_SChar</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">unsigned char</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_UChar</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">WORD</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">short</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Short</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">signed short</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_SShort</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">unsigned short</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_UShort</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">WORD</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">int</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Int</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">signed int</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_SInt</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">unsigned int</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_UInt</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">WORD</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">long</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Long</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">signed long</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_SLong</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">unsigned long</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_ULong</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">WORD</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">long long</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_LongLong</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">signed long long</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_SLongLong</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">unsigned long long</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_ULongLong</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">WORD</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">float</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Float</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">REAL</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">double</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Double</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">REAL</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">size_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Size</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">WORD</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">ptrdiff_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Ptrdiff</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">intmax_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Intmax</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">uintmax_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_UIntmax</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">WORD</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">intptr_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Intptr</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">INTEGER</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">uintptr_t</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_UIntptr</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">WORD</span></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">void *</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">C_Pointer</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">WORD</span></p></td>
</tr>
</tbody>
</table>
<div class="paragraph"><p>These aliases depend on the configuration of the C compiler for the
target architecture, and are independent of the configuration of MLton
(including the <span class="monospaced">-default-type <em>type</em></span>
<a href="CompileTimeOptions"> compiler option</a>).</p></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
