<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>ReleaseChecklist</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>ReleaseChecklist</h1>
</div>
<div id="content">
<div class="sect1">
<h2 id="_advance_preparation_for_release">Advance preparation for release</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
Update <span class="monospaced">./CHANGELOG.adoc</span>.
</p>
<div class="ulist"><ul>
<li>
<p>
Write entries for missing notable commits.
</p>
</li>
<li>
<p>
Write summary of changes from previous release.
</p>
</li>
<li>
<p>
Update with estimated release date.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Update <span class="monospaced">./README.adoc</span>.
</p>
<div class="ulist"><ul>
<li>
<p>
Check features and description.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Update <span class="monospaced">man/{mlton,mlprof}.1</span>.
</p>
<div class="ulist"><ul>
<li>
<p>
Check compile-time and run-time options in <span class="monospaced">man/mlton.1</span>.
</p>
</li>
<li>
<p>
Check options in <span class="monospaced">man/mlprof.1</span>.
</p>
</li>
<li>
<p>
Update with estimated release date.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Update <span class="monospaced">doc/guide</span>.
</p>
<div class="ulist"><ul>
<li>
<p>
Synchronize <a href="Features">Features</a> page with <span class="monospaced">./README.adoc</span>.
</p>
</li>
<li>
<p>
Update <a href="Credits">Credits</a> page with acknowledgements.
</p>
</li>
<li>
<p>
Create <strong>ReleaseYYYYMM??</strong> page (i.e., forthcoming release) based on <strong>ReleaseXXXXLLCC</strong> (i.e., previous release).
</p>
<div class="ulist"><ul>
<li>
<p>
Update summary from <span class="monospaced">./CHANGELOG.adoc</span>.
</p>
</li>
<li>
<p>
Update links to estimated release date.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Create <strong>BugsYYYYMM??</strong> page based on <strong>BugsXXXXLLCC</strong>.
</p>
<div class="ulist"><ul>
<li>
<p>
Update links to estimated release date.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Spell check pages.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Ensure that all updates are pushed to <span class="monospaced">master</span> branch of <a href="https://github.com/MLton/mlton"><span class="monospaced">mlton</span></a>.
</p>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_prepare_sources_for_tagging">Prepare sources for tagging</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
Update <span class="monospaced">./CHANGELOG.adoc</span>.
</p>
<div class="ulist"><ul>
<li>
<p>
Update with proper release date.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Update <span class="monospaced">man/{mlton,mlprof}.1</span>.
</p>
<div class="ulist"><ul>
<li>
<p>
Update with proper release date.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Update <span class="monospaced">doc/guide</span>.
</p>
<div class="ulist"><ul>
<li>
<p>
Rename <strong>ReleaseYYYYMM??</strong> to <strong>ReleaseYYYYMMDD</strong> with proper release date.
</p>
<div class="ulist"><ul>
<li>
<p>
Update links with proper release date.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Rename <strong>BugsYYYYMM??</strong> to <strong>BugsYYYYMMDD</strong> with proper release date.
</p>
<div class="ulist"><ul>
<li>
<p>
Update links with proper release date.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Update <strong>ReleaseXXXXLLCC</strong>.
</p>
<div class="ulist"><ul>
<li>
<p>
Change intro to "<span class="monospaced">This is an archived public release of MLton, version XXXXLLCC.</span>"
</p>
</li>
</ul></div>
</li>
<li>
<p>
Update <a href="Home">Home</a> with note of new release.
</p>
<div class="ulist"><ul>
<li>
<p>
Change <span class="monospaced">What's new?</span> text to <span class="monospaced">Please try out our new release, &lt;:ReleaseYYYYMMDD:MLton YYYYMMDD&gt;</span>.
</p>
</li>
<li>
<p>
Update <span class="monospaced">Download</span> link with proper release date.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Update <a href="Releases">Releases</a> with new release.
</p>
</li>
</ul></div>
</li>
<li>
<p>
Ensure that all updates are pushed to <span class="monospaced">master</span> branch of <a href="https://github.com/MLton/mlton"><span class="monospaced">mlton</span></a>.
</p>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_tag_sources">Tag sources</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
Shell commands:
</p>
<div class="listingblock">
<div class="content monospaced">
<pre>git clone http://github.com/MLton/mlton mlton.git
cd mlton.git
git checkout master
git tag -a -m "Tagging YYYYMMDD release" on-YYYYMMDD-release master
git push origin on-YYYYMMDD-release</pre>
</div></div>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_packaging">Packaging</h2>
<div class="sectionbody">
<div class="sect2">
<h3 id="_sourceforge_frs">SourceForge FRS</h3>
<div class="ulist"><ul>
<li>
<p>
Create <strong>YYYYMMDD</strong> directory:
</p>
<div class="listingblock">
<div class="content monospaced">
<pre>sftp user@frs.sourceforge.net:/home/frs/project/mlton/mlton
sftp&gt; mkdir YYYYMMDD
sftp&gt; quit</pre>
</div></div>
</li>
</ul></div>
</div>
<div class="sect2">
<h3 id="_source_release">Source release</h3>
<div class="ulist"><ul>
<li>
<p>
Create <span class="monospaced">mlton-YYYYMMDD.src.tgz</span>:
</p>
<div class="listingblock">
<div class="content monospaced">
<pre>git clone http://github.com/MLton/mlton mlton
cd mlton
git checkout on-YYYYMMDD-release
make MLTON_VERSION=YYYYMMDD source-release
cd ..</pre>
</div></div>
<div class="paragraph"><p>or</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>wget https://github.com/MLton/mlton/archive/on-YYYYMMDD-release.tar.gz
tar xzvf on-YYYYMMDD-release.tar.gz
cd mlton-on-YYYYMMDD-release
make MLTON_VERSION=YYYYMMDD source-release
cd ..</pre>
</div></div>
</li>
<li>
<p>
Upload <span class="monospaced">mlton-YYYYMMDD.src.tgz</span>:
</p>
<div class="listingblock">
<div class="content monospaced">
<pre>scp mlton-YYYYMMDD.src.tgz user@frs.sourceforge.net:/home/frs/project/mlton/mlton/YYYYMMDD/</pre>
</div></div>
</li>
<li>
<p>
Update <strong>ReleaseYYYYMMDD</strong> with <span class="monospaced">mlton-YYYYMMDD.src.tgz</span> link.
</p>
</li>
</ul></div>
</div>
<div class="sect2">
<h3 id="_binary_releases">Binary releases</h3>
<div class="ulist"><ul>
<li>
<p>
Build and create <span class="monospaced">mlton-YYYYMMDD-1.ARCH-OS.tgz</span>:
</p>
<div class="listingblock">
<div class="content monospaced">
<pre>wget http://sourceforge.net/projects/mlton/files/mlton/YYYYMMDD/mlton-YYYYMMDD.src.tgz
tar xzvf mlton-YYYYMMDD.src.tgz
cd mlton-YYYYMMDD
make binary-release
cd ..</pre>
</div></div>
</li>
<li>
<p>
Upload <span class="monospaced">mlton-YYYYMMDD-1.ARCH-OS.tgz</span>:
</p>
<div class="listingblock">
<div class="content monospaced">
<pre>scp mlton-YYYYMMDD-1.ARCH-OS.tgz user@frs.sourceforge.net:/home/frs/project/mlton/mlton/YYYYMMDD/</pre>
</div></div>
</li>
<li>
<p>
Update <strong>ReleaseYYYYMMDD</strong> with <span class="monospaced">mlton-YYYYMMDD-1.ARCH-OS.tgz</span> link.
</p>
</li>
</ul></div>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_website">Website</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
<span class="monospaced">guide/YYYYMMDD</span> gets a copy of <span class="monospaced">doc/guide/localhost</span>.
</p>
</li>
<li>
<p>
Shell commands:
</p>
<div class="listingblock">
<div class="content monospaced">
<pre>wget http://sourceforge.net/projects/mlton/files/mlton/YYYYMMDD/mlton-YYYYMMDD.src.tgz
tar xzvf mlton-YYYYMMDD.src.tgz
cd mlton-YYYYMMDD
cd doc/guide
cp -prf localhost YYYYMMDD
tar czvf guide-YYYYMMDD.tgz YYYYMMDD
rsync -avzP --delete -e ssh YYYYMMDD user@web.sourceforge.net:/home/project-web/mlton/htdocs/guide/
rsync -avzP --delete -e ssh guide-YYYYMMDD.tgz user@web.sourceforge.net:/home/project-web/mlton/htdocs/guide/</pre>
</div></div>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_announce_release">Announce release</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
Mail announcement to:
</p>
<div class="ulist"><ul>
<li>
<p>
<a href="mailto:MLton-devel@mlton.org"><span class="monospaced">MLton-devel@mlton.org</span></a>
</p>
</li>
<li>
<p>
<a href="mailto:MLton-user@mlton.org"><span class="monospaced">MLton-user@mlton.org</span></a>
</p>
</li>
</ul></div>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_misc">Misc.</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
Generate new <a href="Performance">Performance</a> numbers.
</p>
</li>
</ul></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
