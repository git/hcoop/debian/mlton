<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>WholeProgramOptimization</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>WholeProgramOptimization</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p>Whole-program optimization is a compilation technique in which
optimizations operate over the entire program.  This allows the
compiler many optimization opportunities that are not available when
analyzing modules separately (as with separate compilation).</p></div>
<div class="paragraph"><p>Most of MLton&#8217;s optimizations are whole-program optimizations.
Because MLton compiles the whole program at once, it can perform
optimization across module boundaries.  As a consequence, MLton often
reduces or eliminates the run-time penalty that arises with separate
compilation of SML features such as functors, modules, polymorphism,
and higher-order functions.  MLton takes advantage of having the
entire program to perform transformations such as: defunctorization,
monomorphisation, higher-order control-flow analysis, inlining,
unboxing, argument flattening, redundant-argument removal, constant
folding, and representation selection.  Whole-program compilation is
an integral part of the design of MLton and is not likely to change.</p></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
