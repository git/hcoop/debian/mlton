<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>Bugs20070826</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>Bugs20070826</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p>Here are the known bugs in <a href="Release20070826">MLton 20070826</a>, listed
in reverse chronological order of date reported.</p></div>
<div class="ulist"><ul>
<li>
<p>
<a id="bug25"></a>
Bug in the mark-compact garbage collector where the C library&#8217;s <span class="monospaced">memcpy</span> was used to move objects during the compaction phase; this could lead to heap corruption and segmentation faults with newer versions of gcc and/or glibc, which assume that src and dst in a <span class="monospaced">memcpy</span> do not overlap.
</p>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7461"><span class="monospaced">r7461</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug24"></a>
Bug in elaboration of <span class="monospaced">datatype</span> declarations with <span class="monospaced">withtype</span> bindings.
</p>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7434"><span class="monospaced">r7434</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug23"></a>
Performance bug in <a href="RefFlatten">RefFlatten</a> optimization pass.
</p>
<div class="paragraph"><p>Thanks to Reactive Systems for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7379"><span class="monospaced">r7379</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug22"></a>
Performance bug in <a href="SimplifyTypes">SimplifyTypes</a> optimization pass.
</p>
<div class="paragraph"><p>Thanks to Reactive Systems for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revisions <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7377"><span class="monospaced">r7377</span></a> and <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7378"><span class="monospaced">r7378</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug21"></a>
Bug in amd64 codegen register allocation of indirect C calls.
</p>
<div class="paragraph"><p>Thanks to David Hansel for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7368"><span class="monospaced">r7368</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug20"></a>
Bug in <span class="monospaced">IntInf.scan</span> and <span class="monospaced">IntInf.fromString</span> where leading spaces were only accepted if the stream had an explicit sign character.
</p>
<div class="paragraph"><p>Thanks to David Hansel for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revisions <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7227"><span class="monospaced">r7227</span></a> and <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7230"><span class="monospaced">r7230</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug19"></a>
Bug in <span class="monospaced">IntInf.~&gt;&gt;</span> that could cause a <span class="monospaced">glibc</span> assertion.
</p>
<div class="paragraph"><p>Fixed by revisions <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7083"><span class="monospaced">r7083</span></a>, <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7084"><span class="monospaced">r7084</span></a>, and <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7085"><span class="monospaced">r7085</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug18"></a>
Bug in the return type of <span class="monospaced">MLton.Process.reap</span>.
</p>
<div class="paragraph"><p>Thanks to Risto Saarelma for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r7029"><span class="monospaced">r7029</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug17"></a>
Bug in <span class="monospaced">MLton.size</span> and <span class="monospaced">MLton.share</span> when tracing the current stack.
</p>
<div class="paragraph"><p>Fixed by revisions <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6978"><span class="monospaced">r6978</span></a>, <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6981"><span class="monospaced">r6981</span></a>, <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6988"><span class="monospaced">r6988</span></a>, <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6989"><span class="monospaced">r6989</span></a>, and <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6990"><span class="monospaced">r6990</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug16"></a>
Bug in nested <span class="monospaced">_export</span>/<span class="monospaced">_import</span> functions.
</p>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6919"><span class="monospaced">r6919</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug15"></a>
Bug in the name mangling of <span class="monospaced">_import</span>-ed functions with the <span class="monospaced">stdcall</span> convention.
</p>
<div class="paragraph"><p>Thanks to Lars Bergstrom for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6672"><span class="monospaced">r6672</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug14"></a>
Bug in Windows code to page the heap to disk when unable to grow the heap to a desired size.
</p>
<div class="paragraph"><p>Thanks to Sami Evangelista for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revisions <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6600"><span class="monospaced">r6600</span></a> and <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6624"><span class="monospaced">r6624</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug13"></a>
Bug in \*NIX code to page the heap to disk when unable to grow the heap to a desired size.
</p>
<div class="paragraph"><p>Thanks to Nicolas Bertolotti for the bug report and patch.</p></div>
<div class="paragraph"><p>Fixed by revisions <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6596"><span class="monospaced">r6596</span></a> and <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6600"><span class="monospaced">r6600</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug12"></a>
Space-safety bug in pass to <a href="RefFlatten"> flatten refs</a> into containing data structure.
</p>
<div class="paragraph"><p>Thanks to Daniel Spoonhower for the bug report and initial diagnosis and patch.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6395"><span class="monospaced">r6395</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug11"></a>
Bug in the frontend that rejected <span class="monospaced">op longvid</span> patterns and expressions.
</p>
<div class="paragraph"><p>Thanks to Florian Weimer for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6347"><span class="monospaced">r6347</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug10"></a>
Bug in the <a href="http://www.standardml.org/Basis/imperative-io.html#SIG:IMPERATIVE_IO.canInput:VAL"><span class="monospaced">IMPERATIVE_IO.canInput</span></a> function of the <a href="BasisLibrary">Basis Library</a> implementation.
</p>
<div class="paragraph"><p>Thanks to Ville Laurikari for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6261"><span class="monospaced">r6261</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug09"></a>
Bug in algebraic simplification of real primitives.  <a href="http://www.standardml.org/Basis/real.html#SIG:REAL.\|@LTE\|:VAL"><span class="monospaced">REAL<em>&lt;N&gt;</em>.&lt;=(x, x)</span></a> is <span class="monospaced">false</span> when <span class="monospaced">x</span> is NaN.
</p>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6242"><span class="monospaced">r6242</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug08"></a>
Bug in the FFI visible representation of <span class="monospaced">Int16.int ref</span> (and references of other primitive types smaller than 32-bits) on big-endian platforms.
</p>
<div class="paragraph"><p>Thanks to Dave Herman for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6267"><span class="monospaced">r6267</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug07"></a>
Bug in type inference of flexible records.  This would later cause the compiler to raise the <span class="monospaced">TypeError</span> exception.
</p>
<div class="paragraph"><p>Thanks to Wesley Terpstra for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6229"><span class="monospaced">r6229</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug06"></a>
Bug in cross-compilation of <span class="monospaced">gdtoa</span> library.
</p>
<div class="paragraph"><p>Thanks to Wesley Terpstra for the bug report and patch.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6620"><span class="monospaced">r6620</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug05"></a>
Bug in pass to <a href="RefFlatten"> flatten refs</a> into containing data structure.
</p>
<div class="paragraph"><p>Thanks to Ruy Ley-Wild for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6191"><span class="monospaced">r6191</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug04"></a>
Bug in the handling of weak pointers by the mark-compact garbage collector.
</p>
<div class="paragraph"><p>Thanks to Sean McLaughlin for the bug report and Florian Weimer for the initial diagnosis.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6183"><span class="monospaced">r6183</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug03"></a>
Bug in the elaboration of structures with signature constraints.  This would later cause the compiler to raise the <span class="monospaced">TypeError</span> exception.
</p>
<div class="paragraph"><p>Thanks to Vesa Karvonen for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6046"><span class="monospaced">r6046</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug02"></a>
Bug in the interaction of <span class="monospaced">_export</span>-ed functions and signal handlers.
</p>
<div class="paragraph"><p>Thanks to Sean McLaughlin for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r6013"><span class="monospaced">r6013</span></a>.</p></div>
</li>
<li>
<p>
<a id="bug01"></a>
Bug in the implementation of <span class="monospaced">_export</span>-ed functions using the <span class="monospaced">char</span> type, leading to a linker error.
</p>
<div class="paragraph"><p>Thanks to Katsuhiro Ueno for the bug report.</p></div>
<div class="paragraph"><p>Fixed by revision <a href="https://github.com/MLton/mlton/commit/%3A%2FSVN%20r5999"><span class="monospaced">r5999</span></a>.</p></div>
</li>
</ul></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
