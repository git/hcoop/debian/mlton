<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>PolymorphicEquality</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install(2);
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>PolymorphicEquality</h1>
<div id="toc">
  <div id="toctitle">Table of Contents</div>
  <noscript><p><b>JavaScript must be enabled in your browser to display the table of contents.</b></p></noscript>
</div>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p>Polymorphic equality is a built-in function in
<a href="StandardML">Standard ML</a> that compares two values of the same type
for equality.  It is specified as</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">val</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="n">&#39;&#39;a</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="n">&#39;&#39;a</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">bool</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>The <span class="monospaced">''a</span> in the specification are
<a href="EqualityTypeVariable">equality type variables</a>, and indicate that
polymorphic equality can only be applied to values of an
<a href="EqualityType">equality type</a>.  It is not allowed in SML to rebind
<span class="monospaced">=</span>, so a programmer is guaranteed that <span class="monospaced">=</span> always denotes polymorphic
equality.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_equality_of_ground_types">Equality of ground types</h2>
<div class="sectionbody">
<div class="paragraph"><p>Ground types like <span class="monospaced">char</span>, <span class="monospaced">int</span>, and <span class="monospaced">word</span> may be compared (to values
of the same type).  For example, <span class="monospaced">13 = 14</span> is type correct and yields
<span class="monospaced">false</span>.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_equality_of_reals">Equality of reals</h2>
<div class="sectionbody">
<div class="paragraph"><p>The one ground type that can not be compared is <span class="monospaced">real</span>.  So,
<span class="monospaced">13.0 = 14.0</span> is not type correct.  One can use <span class="monospaced">Real.==</span> to compare
reals for equality, but beware that this has different algebraic
properties than polymorphic equality.</p></div>
<div class="paragraph"><p>See <a href="http://standardml.org/Basis/real.html">http://standardml.org/Basis/real.html</a> for a discussion of why
<span class="monospaced">real</span> is not an equality type.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_equality_of_functions">Equality of functions</h2>
<div class="sectionbody">
<div class="paragraph"><p>Comparison of functions is not allowed.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_equality_of_immutable_types">Equality of immutable types</h2>
<div class="sectionbody">
<div class="paragraph"><p>Polymorphic equality can be used on <a href="Immutable">immutable</a> values like
tuples, records, lists, and vectors.  For example,</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>(1, 2, 3) = (4, 5, 6)</pre>
</div></div>
<div class="paragraph"><p>is a type-correct expression yielding <span class="monospaced">false</span>, while</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>[1, 2, 3] = [1, 2, 3]</pre>
</div></div>
<div class="paragraph"><p>is type correct and yields <span class="monospaced">true</span>.</p></div>
<div class="paragraph"><p>Equality on immutable values is computed by structure, which means
that values are compared by recursively descending the data structure
until ground types are reached, at which point the ground types are
compared with primitive equality tests (like comparison of
characters).  So, the expression</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>[1, 2, 3] = [1, 1 + 1, 1 + 1 + 1]</pre>
</div></div>
<div class="paragraph"><p>is guaranteed to yield <span class="monospaced">true</span>, even though the lists may occupy
different locations in memory.</p></div>
<div class="paragraph"><p>Because of structural equality, immutable values can only be compared
if their components can be compared.  For example, <span class="monospaced">[1, 2, 3]</span> can be
compared, but <span class="monospaced">[1.0, 2.0, 3.0]</span> can not.  The SML type system uses
<a href="EqualityType">equality types</a> to ensure that structural equality is
only applied to valid values.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_equality_of_mutable_values">Equality of mutable values</h2>
<div class="sectionbody">
<div class="paragraph"><p>In contrast to immutable values, polymorphic equality of
<a href="Mutable">mutable</a> values (like ref cells and arrays) is performed by
pointer comparison, not by structure.  So, the expression</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>ref 13 = ref 13</pre>
</div></div>
<div class="paragraph"><p>is guaranteed to yield <span class="monospaced">false</span>, even though the ref cells hold the
same contents.</p></div>
<div class="paragraph"><p>Because equality of mutable values is not structural, arrays and refs
can be compared <em>even if their components are not equality types</em>.
Hence, the following expression is type correct (and yields true).</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">let</span><span class="w"></span>
<span class="w">   </span><span class="k">val</span><span class="w"> </span><span class="n">r</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">ref</span><span class="w"> </span><span class="mf">13.0</span><span class="w"></span>
<span class="k">in</span><span class="w"></span>
<span class="w">   </span><span class="n">r</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">r</span><span class="w"></span>
<span class="k">end</span><span class="w"></span>
</pre></div></div></div>
</div>
</div>
<div class="sect1">
<h2 id="_equality_of_datatypes">Equality of datatypes</h2>
<div class="sectionbody">
<div class="paragraph"><p>Polymorphic equality of datatypes is structural.  Two values of the
same datatype are equal if they are of the same <a href="Variant">variant</a> and
if the <a href="Variant">variant</a>'s arguments are equal (recursively).  So,
with the datatype</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">datatype</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">A</span><span class="w"> </span><span class="p">|</span><span class="w"> </span><span class="n">B</span><span class="w"> </span><span class="k">of</span><span class="w"> </span><span class="n">t</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>then <span class="monospaced">B (B A) = B A</span> is type correct and yields <span class="monospaced">false</span>, while <span class="monospaced">A = A</span>
and <span class="monospaced">B A = B A</span> yield <span class="monospaced">true</span>.</p></div>
<div class="paragraph"><p>As polymorphic equality descends two values to compare them, it uses
pointer equality whenever it reaches a mutable value.  So, with the
datatype</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">datatype</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">A</span><span class="w"> </span><span class="k">of</span><span class="w"> </span><span class="n">int</span><span class="w"> </span><span class="n">ref</span><span class="w"> </span><span class="p">|</span><span class="w"> </span><span class="p">...</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>then <span class="monospaced">A (ref 13) = A (ref 13)</span> is type correct and yields <span class="monospaced">false</span>,
because the pointer equality on the two ref cells yields <span class="monospaced">false</span>.</p></div>
<div class="paragraph"><p>One weakness of the SML type system is that datatypes do not inherit
the special property of the <span class="monospaced">ref</span> and <span class="monospaced">array</span> type constructors that
allows them to be compared regardless of their component type.  For
example, after declaring</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">datatype</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">A</span><span class="w"> </span><span class="k">of</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">ref</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>one might expect to be able to compare two values of type <span class="monospaced">real t</span>,
because pointer comparison on a ref cell would suffice.
Unfortunately, the type system can only express that a user-defined
datatype <a href="AdmitsEquality">admits equality</a> or not.  In this case, <span class="monospaced">t</span>
admits equality, which means that <span class="monospaced">int t</span> can be compared but that
<span class="monospaced">real t</span> can not.  We can confirm this with the program</p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">datatype</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">A</span><span class="w"> </span><span class="k">of</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="n">ref</span><span class="w"></span>
<span class="k">fun</span><span class="w"> </span><span class="n">f</span><span class="w"> </span><span class="p">(</span><span class="n">x</span><span class="p">:</span><span class="w"> </span><span class="n">real</span><span class="w"> </span><span class="n">t</span><span class="p">,</span><span class="w"> </span><span class="n">y</span><span class="p">:</span><span class="w"> </span><span class="n">real</span><span class="w"> </span><span class="n">t</span><span class="p">)</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">x</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">y</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p>on which MLton reports the following error.</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>Error: z.sml 2.32-2.36.
  Function applied to incorrect argument.
    expects: [&lt;equality&gt;] t * [&lt;equality&gt;] t
    but got: [real] t * [real] t
    in: = (x, y)</pre>
</div></div>
</div>
</div>
<div class="sect1">
<h2 id="_implementation">Implementation</h2>
<div class="sectionbody">
<div class="paragraph"><p>Polymorphic equality is implemented by recursively descending the two
values being compared, stopping as soon as they are determined to be
unequal, or exploring the entire values to determine that they are
equal.  Hence, polymorphic equality can take time proportional to the
size of the smaller value.</p></div>
<div class="paragraph"><p>MLton uses some optimizations to improve performance.</p></div>
<div class="ulist"><ul>
<li>
<p>
When computing structural equality, first do a pointer comparison.
If the comparison yields <span class="monospaced">true</span>, then stop and return <span class="monospaced">true</span>, since
the structural comparison is guaranteed to do so.  If the pointer
comparison fails, then recursively descend the values.
</p>
</li>
<li>
<p>
If a datatype is an enum (e.g. <span class="monospaced">datatype t = A | B | C</span>), then a
single comparison suffices to compare values of the datatype.  No case
dispatch is required to determine whether the two values are of the
same <a href="Variant">variant</a>.
</p>
</li>
<li>
<p>
When comparing a known constant non-value-carrying
<a href="Variant">variant</a>, use a single comparison.  For example, the
following code will compile into a single comparison for <span class="monospaced">A = x</span>.
</p>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">datatype</span><span class="w"> </span><span class="n">t</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">A</span><span class="w"> </span><span class="p">|</span><span class="w"> </span><span class="n">B</span><span class="w"> </span><span class="p">|</span><span class="w"> </span><span class="n">C</span><span class="w"> </span><span class="k">of</span><span class="w"> </span><span class="p">...</span><span class="w"></span>
<span class="k">fun</span><span class="w"> </span><span class="n">f</span><span class="w"> </span><span class="n">x</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="p">...</span><span class="w"> </span><span class="k">if</span><span class="w"> </span><span class="n">A</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">x</span><span class="w"> </span><span class="k">then</span><span class="w"> </span><span class="p">...</span><span class="w"></span>
</pre></div></div></div>
</li>
<li>
<p>
When comparing a small constant <span class="monospaced">IntInf.int</span> to another
<span class="monospaced">IntInf.int</span>, use a single comparison against the constant.  No case
dispatch is required.
</p>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_also_see">Also see</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
<a href="AdmitsEquality">AdmitsEquality</a>
</p>
</li>
<li>
<p>
<a href="EqualityType">EqualityType</a>
</p>
</li>
<li>
<p>
<a href="EqualityTypeVariable">EqualityTypeVariable</a>
</p>
</li>
</ul></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
