<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>MLRISCLibrary</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>MLRISCLibrary</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p>The <a href="http://www.cs.nyu.edu/leunga/www/MLRISC/Doc/html/index.html">MLRISC
Library</a> is a framework for retargetable and optimizing compiler back
ends.  The MLRISC Library is distributed with SML/NJ.  Due to
differences between SML/NJ and MLton, this library will not work
out-of-the box with MLton.</p></div>
<div class="paragraph"><p>As of 20180119, MLton includes a port of the MLRISC Library
synchronized with SML/NJ version 110.82.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_usage">Usage</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
You can import a sub-library of the MLRISC Library into an MLB file with:
</p>
<table class="tableblock frame-all grid-all"
style="
width:100%;
">
<col style="width:50%;">
<col style="width:50%;">
<thead>
<tr>
<th class="tableblock halign-left valign-top" >MLB file</th>
<th class="tableblock halign-left valign-top" >Description</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/ALPHA.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock">The ALPHA backend</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/AMD64.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock">The AMD64 backend</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/AMD64-Peephole.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock">The AMD64 peephole optimizer</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/CCall.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/CCall-sparc.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/CCall-x86-64.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/CCall-x86.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/Control.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/Graphs.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/HPPA.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock">The HPPA backend</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/IA32.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock">The IA32 backend</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/IA32-Peephole.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock">The IA32 peephole optimizer</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/Lib.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/MLRISC.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/MLTREE.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/Peephole.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/PPC.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock">The PPC backend</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/RA.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/SPARC.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock">The Sparc backend</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/StagedAlloc.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top" ><p class="tableblock"><span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/Visual.mlb</span></p></td>
<td class="tableblock halign-left valign-top" ><p class="tableblock"></p></td>
</tr>
</tbody>
</table>
</li>
<li>
<p>
If you are porting a project from SML/NJ&#8217;s <a href="CompilationManager">CompilationManager</a> to
MLton&#8217;s <a href="MLBasis"> ML Basis system</a> using <span class="monospaced">cm2mlb</span>, note that the
following map is included by default:
</p>
<div class="listingblock">
<div class="content monospaced">
<pre># MLRISC Library
$SMLNJ-MLRISC                           $(SML_LIB)/mlrisc-lib/mlb</pre>
</div></div>
<div class="paragraph"><p>This will automatically convert a <span class="monospaced">$SMLNJ-MLRISC/MLRISC.cm</span> import in
an input <span class="monospaced">.cm</span> file into a <span class="monospaced">$(SML_LIB)/mlrisc-lib/mlb/MLRISC.mlb</span>
import in the output <span class="monospaced">.mlb</span> file.</p></div>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_details">Details</h2>
<div class="sectionbody">
<div class="paragraph"><p>The following changes were made to the MLRISC Library, in addition to
deriving the <span class="monospaced">.mlb</span> files from the <span class="monospaced">.cm</span> files:</p></div>
<div class="ulist"><ul>
<li>
<p>
eliminate sequential <span class="monospaced">withtype</span> expansions: Most could be rewritten as a sequence of type definitions and datatype definitions.
</p>
</li>
<li>
<p>
eliminate higher-order functors: Every higher-order functor definition and application could be uncurried in the obvious way.
</p>
</li>
<li>
<p>
eliminate <span class="monospaced">where &lt;str&gt; = &lt;str&gt;</span>: Quite painful to expand out all the flexible types in the respective structures.  Furthermore, many of the implied type equalities aren&#8217;t needed, but it&#8217;s too hard to pick out the right ones.
</p>
</li>
<li>
<p>
<span class="monospaced">library/array-noneq.sml</span> (added, not exported): Implements <span class="monospaced">signature ARRAY_NONEQ</span>, similar to <span class="monospaced">signature ARRAY</span> from the <a href="BasisLibrary">Basis Library</a>, but replacing the latter&#8217;s <span class="monospaced">eqtype 'a array = 'a array</span> and <span class="monospaced">type 'a vector = 'a Vector.vector</span> with <span class="monospaced">type 'a array</span> and <span class="monospaced">type 'a vector</span>.  Thus, array-like containers may match <span class="monospaced">ARRAY_NONEQ</span>, whereas only the pervasive <span class="monospaced">'a array</span> container may math <span class="monospaced">ARRAY</span>.  (SML/NJ&#8217;s implementation of <span class="monospaced">signature ARRAY</span> omits the type realizations.)
</p>
</li>
<li>
<p>
<span class="monospaced">library/dynamic-array.sml</span> and <span class="monospaced">library/hash-array.sml</span> (modifed): Replace <span class="monospaced">include ARRAY</span> with <span class="monospaced">include ARRAY_NONEQ</span>; see above.
</p>
</li>
</ul></div>
</div>
</div>
<div class="sect1">
<h2 id="_patch">Patch</h2>
<div class="sectionbody">
<div class="ulist"><ul>
<li>
<p>
<a href="https://github.com/MLton/mlton/blob/master/lib/mlrisc-lib/MLRISC.patch"><span class="monospaced">MLRISC.patch</span></a>
</p>
</li>
</ul></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
