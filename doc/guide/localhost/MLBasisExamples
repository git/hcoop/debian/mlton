<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="generator" content="AsciiDoc 8.6.9">
<title>MLBasisExamples</title>
<link rel="stylesheet" href="./asciidoc.css" type="text/css">
<link rel="stylesheet" href="./pygments.css" type="text/css">


<script type="text/javascript" src="./asciidoc.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
asciidoc.install();
/*]]>*/
</script>
<link rel="stylesheet" href="./mlton.css" type="text/css">
</head>
<body class="article">
<div id="banner">
<div id="banner-home">
<a href="./Home">MLton 20180207</a>
</div>
</div>
<div id="header">
<h1>MLBasisExamples</h1>
</div>
<div id="content">
<div id="preamble">
<div class="sectionbody">
<div class="paragraph"><p>Here are some example uses of <a href="MLBasis">ML Basis</a> files.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_complete_program">Complete program</h2>
<div class="sectionbody">
<div class="paragraph"><p>Suppose your complete program consists of the files <span class="monospaced">file1.sml</span>, &#8230;,
<span class="monospaced">filen.sml</span>, which depend upon libraries <span class="monospaced">lib1.mlb</span>, &#8230;, <span class="monospaced">libm.mlb</span>.</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>(* import libraries *)
lib1.mlb
...
libm.mlb

(* program files *)
file1.sml
...
filen.sml</pre>
</div></div>
<div class="paragraph"><p>The bases denoted by <span class="monospaced">lib1.mlb</span>, &#8230;, <span class="monospaced">libm.mlb</span> are merged (bindings
of names in later bases take precedence over bindings of the same name
in earlier bases), producing a basis in which <span class="monospaced">file1.sml</span>, &#8230;,
<span class="monospaced">filen.sml</span> are elaborated, adding additional bindings to the basis.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_export_filter">Export filter</h2>
<div class="sectionbody">
<div class="paragraph"><p>Suppose you only want to export certain structures, signatures, and
functors from a collection of files.</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>local
  file1.sml
  ...
  filen.sml
in
  (* export filter here *)
  functor F
  structure S
end</pre>
</div></div>
<div class="paragraph"><p>While <span class="monospaced">file1.sml</span>, &#8230;, <span class="monospaced">filen.sml</span> may declare top-level identifiers
in addition to <span class="monospaced">F</span> and <span class="monospaced">S</span>, such names are not accessible to programs
and libraries that import this <span class="monospaced">.mlb</span>.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_export_filter_with_renaming">Export filter with renaming</h2>
<div class="sectionbody">
<div class="paragraph"><p>Suppose you want an export filter, but want to rename one of the
modules.</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>local
  file1.sml
  ...
  filen.sml
in
  (* export filter, with renaming, here *)
  functor F
  structure S' = S
end</pre>
</div></div>
<div class="paragraph"><p>Note that <span class="monospaced">functor F</span> is an abbreviation for <span class="monospaced">functor F = F</span>, which
simply exports an identifier under the same name.</p></div>
</div>
</div>
<div class="sect1">
<h2 id="_import_filter">Import filter</h2>
<div class="sectionbody">
<div class="paragraph"><p>Suppose you only want to import a functor <span class="monospaced">F</span> from one library and a
structure <span class="monospaced">S</span> from another library.</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>local
  lib1.mlb
in
  (* import filter here *)
  functor F
end
local
  lib2.mlb
in
  (* import filter here *)
  structure S
end
file1.sml
...
filen.sml</pre>
</div></div>
</div>
</div>
<div class="sect1">
<h2 id="_import_filter_with_renaming">Import filter with renaming</h2>
<div class="sectionbody">
<div class="paragraph"><p>Suppose you want to import a structure <span class="monospaced">S</span> from one library and
another structure <span class="monospaced">S</span> from another library.</p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>local
  lib1.mlb
in
  (* import filter, with renaming, here *)
  structure S1 = S
end
local
  lib2.mlb
in
  (* import filter, with renaming, here *)
  structure S2 = S
end
file1.sml
...
filen.sml</pre>
</div></div>
</div>
</div>
<div class="sect1">
<h2 id="_full_basis">Full Basis</h2>
<div class="sectionbody">
<div class="paragraph"><p>Since the Modules level of SML is the natural means for organizing
program and library components, MLB files provide convenient syntax
for renaming Modules level identifiers (in fact, renaming of functor
identifiers provides a mechanism that is not available in SML).
However, please note that <span class="monospaced">.mlb</span> files elaborate to full bases
including top-level types and values (including infix status), in
addition to structures, signatures, and functors.  For example,
suppose you wished to extend the <a href="BasisLibrary">Basis Library</a> with an
<span class="monospaced">('a, 'b) either</span> datatype corresponding to a disjoint sum; the type
and some operations should be available at the top-level;
additionally, a signature and structure provide the complete
interface.</p></div>
<div class="paragraph"><p>We could use the following files.</p></div>
<div class="paragraph"><p><span class="monospaced">either-sigs.sml</span></p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">signature</span><span class="w"> </span><span class="n">EITHER_GLOBAL</span><span class="w"> </span><span class="p">=</span><span class="w"></span>
<span class="w">  </span><span class="k">sig</span><span class="w"></span>
<span class="w">    </span><span class="k">datatype</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;b</span><span class="p">)</span><span class="w"> </span><span class="n">either</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">Left</span><span class="w"> </span><span class="k">of</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="p">|</span><span class="w"> </span><span class="n">Right</span><span class="w"> </span><span class="k">of</span><span class="w"> </span><span class="n">&#39;b</span><span class="w"></span>
<span class="w">    </span><span class="k">val</span><span class="w"> </span><span class="n">&amp;</span><span class="w">  </span><span class="p">:</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">&#39;c</span><span class="p">)</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;b</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">&#39;c</span><span class="p">)</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;b</span><span class="p">)</span><span class="w"> </span><span class="n">either</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">&#39;c</span><span class="w"></span>
<span class="w">    </span><span class="k">val</span><span class="w"> </span><span class="n">&amp;&amp;</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">&#39;c</span><span class="p">)</span><span class="w"> </span><span class="n">*</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;b</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">&#39;d</span><span class="p">)</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;b</span><span class="p">)</span><span class="w"> </span><span class="n">either</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;c</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;d</span><span class="p">)</span><span class="w"> </span><span class="n">either</span><span class="w"></span>
<span class="w">  </span><span class="k">end</span><span class="w"></span>

<span class="k">signature</span><span class="w"> </span><span class="n">EITHER</span><span class="w"> </span><span class="p">=</span><span class="w"></span>
<span class="w">  </span><span class="k">sig</span><span class="w"></span>
<span class="w">    </span><span class="k">include</span><span class="w"> </span><span class="n">EITHER_GLOBAL</span><span class="w"></span>
<span class="w">    </span><span class="k">val</span><span class="w"> </span><span class="n">isLeft</span><span class="w">  </span><span class="p">:</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;b</span><span class="p">)</span><span class="w"> </span><span class="n">either</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">bool</span><span class="w"></span>
<span class="w">    </span><span class="k">val</span><span class="w"> </span><span class="n">isRight</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;b</span><span class="p">)</span><span class="w"> </span><span class="n">either</span><span class="w"> </span><span class="p">-&gt;</span><span class="w"> </span><span class="n">bool</span><span class="w"></span>
<span class="w">    </span><span class="p">...</span><span class="w"></span>
<span class="w">  </span><span class="k">end</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p><span class="monospaced">either-strs.sml</span></p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">structure</span><span class="w"> </span><span class="n">Either</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="n">EITHER</span><span class="w"> </span><span class="p">=</span><span class="w"></span>
<span class="w">  </span><span class="k">struct</span><span class="w"></span>
<span class="w">    </span><span class="k">datatype</span><span class="w"> </span><span class="p">(</span><span class="n">&#39;a</span><span class="p">,</span><span class="w"> </span><span class="n">&#39;b</span><span class="p">)</span><span class="w"> </span><span class="n">either</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">Left</span><span class="w"> </span><span class="k">of</span><span class="w"> </span><span class="n">&#39;a</span><span class="w"> </span><span class="p">|</span><span class="w"> </span><span class="n">Right</span><span class="w"> </span><span class="k">of</span><span class="w"> </span><span class="n">&#39;b</span><span class="w"></span>
<span class="w">    </span><span class="k">fun</span><span class="w"> </span><span class="n">f</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="n">g</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="k">fn</span><span class="w"> </span><span class="n">x</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"></span>
<span class="w">      </span><span class="k">case</span><span class="w"> </span><span class="n">x</span><span class="w"> </span><span class="k">of</span><span class="w"> </span><span class="n">Left</span><span class="w"> </span><span class="n">z</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="n">f</span><span class="w"> </span><span class="n">z</span><span class="w"> </span><span class="p">|</span><span class="w"> </span><span class="n">Right</span><span class="w"> </span><span class="n">z</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="n">g</span><span class="w"> </span><span class="n">z</span><span class="w"></span>
<span class="w">    </span><span class="k">fun</span><span class="w"> </span><span class="n">f</span><span class="w"> </span><span class="n">&amp;&amp;</span><span class="w"> </span><span class="n">g</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="p">(</span><span class="n">Left</span><span class="w"> </span><span class="n">o</span><span class="w"> </span><span class="n">f</span><span class="p">)</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="p">(</span><span class="n">Right</span><span class="w"> </span><span class="n">o</span><span class="w"> </span><span class="n">g</span><span class="p">)</span><span class="w"></span>
<span class="w">    </span><span class="k">fun</span><span class="w"> </span><span class="n">isLeft</span><span class="w"> </span><span class="n">x</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="p">((</span><span class="k">fn</span><span class="w"> </span><span class="p">_</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="n">true</span><span class="p">)</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="p">(</span><span class="k">fn</span><span class="w"> </span><span class="p">_</span><span class="w"> </span><span class="p">=&gt;</span><span class="w"> </span><span class="n">false</span><span class="p">))</span><span class="w"> </span><span class="n">x</span><span class="w"></span>
<span class="w">    </span><span class="k">fun</span><span class="w"> </span><span class="n">isRight</span><span class="w"> </span><span class="n">x</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="p">(</span><span class="n">not</span><span class="w"> </span><span class="n">o</span><span class="w"> </span><span class="n">isLeft</span><span class="p">)</span><span class="w"> </span><span class="n">x</span><span class="w"></span>
<span class="w">    </span><span class="p">...</span><span class="w"></span>
<span class="w">  </span><span class="k">end</span><span class="w"></span>
<span class="k">structure</span><span class="w"> </span><span class="n">EitherGlobal</span><span class="w"> </span><span class="p">:</span><span class="w"> </span><span class="n">EITHER_GLOBAL</span><span class="w"> </span><span class="p">=</span><span class="w"> </span><span class="n">Either</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p><span class="monospaced">either-infixes.sml</span></p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">infixr</span><span class="w"> </span><span class="mi">3</span><span class="w"> </span><span class="n">&amp;</span><span class="w"> </span><span class="n">&amp;&amp;</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p><span class="monospaced">either-open.sml</span></p></div>
<div class="listingblock">
<div class="content"><div class="highlight"><pre><span class="k">open</span><span class="w"> </span><span class="n">EitherGlobal</span><span class="w"></span>
</pre></div></div></div>
<div class="paragraph"><p><span class="monospaced">either.mlb</span></p></div>
<div class="listingblock">
<div class="content monospaced">
<pre>either-infixes.sml
local
  (* import Basis Library *)
  $(SML_LIB)/basis/basis.mlb
  either-sigs.sml
  either-strs.sml
in
  signature EITHER
  structure Either
  either-open.sml
end</pre>
</div></div>
<div class="paragraph"><p>A client that imports <span class="monospaced">either.mlb</span> will have access to neither
<span class="monospaced">EITHER_GLOBAL</span> nor <span class="monospaced">EitherGlobal</span>, but will have access to the type
<span class="monospaced">either</span> and the values <span class="monospaced">&amp;</span> and <span class="monospaced">&amp;&amp;</span> (with infix status) in the
top-level environment.  Note that <span class="monospaced">either-infixes.sml</span> is outside the
scope of the local, because we want the infixes available in the
implementation of the library and to clients of the library.</p></div>
</div>
</div>
</div>
<div id="footnotes"><hr></div>
<div id="footer">
<div id="footer-text">
</div>
<div id="footer-badges">
</div>
</div>
</body>
</html>
